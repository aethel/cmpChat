'use strict';

/**
 * @ngdoc overview
 * @name cmpApp
 * @description
 * # cmpApp
 *
 * Main module of the application.
 */
angular
	.module('cmpApp', [
		'ngAnimate',
		'ngCookies',
		'ngResource',
		'ngRoute',
		'ngSanitize',
		'ngTouch',
		'cmpApp.profile',
		'cmpApp.chat'
	])
	.config(["$routeProvider", function ($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'components/profile/profile.html',
				controller: 'ProfileCtrl',
				controllerAs: 'profile'
			})
			.when('/chat:user', {
				templateUrl: 'components/chat/chat.html',
				controller: 'ChatCtrl',
				controllerAs: 'chat'
			})
			.otherwise({
				redirectTo: '/'
			});
	}]);

'use strict';

/**
 * @ngdoc function
 * @name cmpApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the cmpApp
 */
angular.module('cmpApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name cmpApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the cmpApp
 */
angular.module('cmpApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name cmpApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the cmpApp
 */
angular.module('cmpApp.profile',[])
	.controller('ProfileCtrl', ['$scope', function ($scope) {
		var befriendBtn = document.querySelector('.js-befriend-btn'),
				container = document.querySelector('.js-container');

		befriendBtn.addEventListener('click', function(){
			container.classList.toggle('is-befriended');
		});
	}]);

'use strict';

/**
 * @ngdoc function
 * @name cmpApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the cmpApp
 */
angular.module('cmpApp.chat', ['ngRoute', 'ngStorage'])
	.controller('ChatCtrl', ['$routeParams', '$scope', '$sessionStorage', function ($routeParams, $scope, $sessionStorage) {
		var self = this;

		self.thisUser = {
			id: 'this321'
		};

		self.otherUser = {
			id: $routeParams.user
		}
		self.initialMessages = [
			'Hello sailor. What are you up to?'

			, 'Hey, how goes it?'

			, 'Busy?'

			, 'So, what\'s up?'
		];

		self.randomMessage = self.initialMessages[Math.floor(Math.random() * self.initialMessages.length)];
		self.$storage = $sessionStorage;
		if(self.$storage.chat == undefined) {
			self.$storage.chat = [];
			self.$storage.chat.push({
					 user: {
						id: self.otherUser.id,
						answer: self.randomMessage
					}
		});
		}
		self.reply;

		self.submit = function () {
			if (self.reply) {
				self.$storage.chat.push({
					 user: {
						id: self.thisUser.id,
						answer: self.reply
					}
				});
				self.reply = '';
			}
		}
	}]);

angular.module('cmpApp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/about.html',
    "<p>This is the about view.</p>"
  );


  $templateCache.put('views/main.html',
    "<div class=\"jumbotron\"> <h1>'Allo, 'Allo!</h1> <p class=\"lead\"> <img src=\"images/yeoman.png\" alt=\"I'm Yeoman\"><br> Always a pleasure scaffolding your apps. </p> <p><a class=\"btn btn-lg btn-success\" ng-href=\"#/\">Splendid!<span class=\"glyphicon glyphicon-ok\"></span></a></p> </div> <div class=\"row marketing\"> <h4>HTML5 Boilerplate</h4> <p> HTML5 Boilerplate is a professional front-end template for building fast, robust, and adaptable web apps or sites. </p> <h4>Angular</h4> <p> AngularJS is a toolset for building the framework most suited to your application development. </p> <h4>Karma</h4> <p>Spectacular Test Runner for JavaScript.</p> </div>"
  );

}]);
