'use strict';

/**
 * @ngdoc overview
 * @name cmpApp
 * @description
 * # cmpApp
 *
 * Main module of the application.
 */
angular
	.module('cmpApp', [
		'ngAnimate',
		'ngCookies',
		'ngResource',
		'ngRoute',
		'ngSanitize',
		'ngTouch',
		'cmpApp.profile',
		'cmpApp.chat'
	])
	.config(function ($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'components/profile/profile.html',
				controller: 'ProfileCtrl',
				controllerAs: 'profile'
			})
			.when('/chat:user', {
				templateUrl: 'components/chat/chat.html',
				controller: 'ChatCtrl',
				controllerAs: 'chat'
			})
			.otherwise({
				redirectTo: '/'
			});
	});
