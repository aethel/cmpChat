'use strict';

/**
 * @ngdoc function
 * @name cmpApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the cmpApp
 */
angular.module('cmpApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
