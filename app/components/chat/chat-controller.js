'use strict';

/**
 * @ngdoc function
 * @name cmpApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the cmpApp
 */
angular.module('cmpApp.chat', ['ngRoute', 'ngStorage'])
	.controller('ChatCtrl', ['$routeParams', '$scope', '$sessionStorage', function ($routeParams, $scope, $sessionStorage) {
		var self = this;

		self.thisUser = {
			id: 'this321'
		};

		self.otherUser = {
			id: $routeParams.user
		}
		self.initialMessages = [
			'Hello sailor. What are you up to?'

			, 'Hey, how goes it?'

			, 'Busy?'

			, 'So, what\'s up?'
		];

		self.randomMessage = self.initialMessages[Math.floor(Math.random() * self.initialMessages.length)];
		self.$storage = $sessionStorage;
		if(self.$storage.chat == undefined) {
			self.$storage.chat = [];
			self.$storage.chat.push({
					 user: {
						id: self.otherUser.id,
						answer: self.randomMessage
					}
		});
		}
		self.reply;

		self.submit = function () {
			if (self.reply) {
				self.$storage.chat.push({
					 user: {
						id: self.thisUser.id,
						answer: self.reply
					}
				});
				self.reply = '';
			}
		}
	}]);
