'use strict';

/**
 * @ngdoc function
 * @name cmpApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the cmpApp
 */
angular.module('cmpApp.profile',[])
	.controller('ProfileCtrl', ['$scope', function ($scope) {
		var befriendBtn = document.querySelector('.js-befriend-btn'),
				container = document.querySelector('.js-container');

		befriendBtn.addEventListener('click', function(){
			container.classList.toggle('is-befriended');
		});
	}]);
